#!/bin/bash

sudo apt update -y && sudo apt install \
	vim git openssh-client openssh-server \
	docker.io gnome-tweaks snapd

# Create aliases (see WSL2 env)

# You needd gnome-tweaks to add the maximize button

# Git settings
git config --global user.name "Peter Zandbergen"
git config --global user.email "peter.zandbergen@myhops.com"
git config --global core.editor vim

# Install Chrome using Firefox

# Install Go
export GOLANG_TAR="go1.17.5.linux-amd64.tar.gz"
curl --location "https://go.dev/dl/$GOLANG_TAR" -o "/tmp/$GOLANG_TAR"
sudo rm -rf /usr/local/go
sudo -E tar -C /usr/local -xzf /tmp/$GOLANG_TAR

# minikube kubectl kustomize powerline-go fonts for powerline-go
# docker of podman

# Helm
curl -L -o /tmp/helm.tar.gz https://get.helm.sh/helm-v3.7.2-linux-amd64.tar.gz
mkdir -p /tmp/helm
tar  -C /tmp/helm -xzf /tmp/helm.tar.gz
sudo cp /tmp/helm/linux-amd64/helm /usr/local/bin

# Minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

# Kubectl
export KUBECTL=/usr/local/bin/kubectl
sudo -E -- curl -L -o $KUBECTL "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
sudo -E -- chmod a+x $KUBECTL

# Kustomize
curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
sudo mv kustomize /usr/local/bin

# k9s
curl -L -o /tmp/k9s.tar.gz https://github.com/derailed/k9s/releases/download/v0.25.16/k9s_Linux_x86_64.tar.gz
tar -C /tmp -xf /tmp/k9s.tar.gz
sudo cp /tmp/k9s /usr/local/bin

# Powerline
go install github.com/justjanne/powerline-go@latest
# Install in .bashrc
cat <<'EOF' >> $HOME/.bashrc

# Powerline go
GOPATH=$HOME/go
# Powerline Go
function _update_ps1() {
    PS1="$($GOPATH/bin/powerline-go -error $? -jobs $(jobs -p | wc -l))"

    # Uncomment the following line to automatically clear errors after showing
    # them once. This not only clears the error for powerline-go, but also for
    # everything else you run in that shell. Don't enable this if you're not
    # sure this is what you want.

    #set "?"
}

if [ "$TERM" != "linux" ] && [ -f "$GOPATH/bin/powerline-go" ]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi
EOF

# .vimrc with swap, dir and backupdir

# background image instellen

# Go to https://snapcraft.io/authy
sudo snap install authy

# Install Visual Studio Code vanuit Shop

# Bash completion
# Put files with the same name as the exec in ~/.local/share/bash-completion/completions

